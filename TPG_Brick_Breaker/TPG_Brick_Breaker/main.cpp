#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>

#define BORDER_OFFSET 50
#define PANEL_WIDTH   80
#define PANEL_HEIGHT  10
#define PANEL_JUMP	  4
#define BALL_DIAMETER 10
#define BALL_START_HT 200
#define BALL_JUMP	  4
#define BALL_FREQ	  50
#define BLOCK_HEIGHT  10
#define BLOCK_WIDTH   20
#define BLOCK_OFFSET  52
#define BLOCK_Y		  10
#define BLOCK_X		  20
#define BLOCK_DIV	  1
#define BRICK_RED	  RGB(203, 65, 84)
#define PEN_LEN		  1
#define SCREEN_WIDTH  (BORDER_OFFSET * 2) + (BLOCK_X * BLOCK_WIDTH) + ((BLOCK_X - 1) * BLOCK_DIV) + (BLOCK_OFFSET * 2) + (PEN_LEN * 2)
#define SCREEN_HEIGHT 500

static TCHAR szWindowClass[] = _T("TPG Brick Breaker App");
static TCHAR szTitle[] = _T("TPG Brick Breaker Application");

enum BALL_DIRECTION
{
	DIR_SW = 0,
	DIR_NW = 1,
	DIR_NE = 2,
	DIR_SE = 3
};

enum PANEL_DIRECTION
{
	SLIDE_LEFT  = 0,
	SLIDE_RIGHT = 1
};

HINSTANCE hInst;
static int lives;
static BOOL running, draw_blocks, draw_border, draw_panel, draw_ball, set_info;
static RECT border, panel, ball, block;
static BALL_DIRECTION dir;
static BOOL blocks[BLOCK_Y][BLOCK_X];

void check_panel_collision()
{
	//ball falling into panel
	//ball must be falling, don't check if ball is traveling back upward
	if ((dir == DIR_SW) || (dir == DIR_SE))
	{
		//ball is at same height as panel
		if ((ball.bottom >= panel.top) && (ball.bottom < panel.bottom))
		{
			//rightmost of ball is touching leftmost of panel and vice versa
			if ((ball.right >= panel.left) && (ball.left <= panel.right))
			{
				if (dir == DIR_SW)
					dir = DIR_NW;
				else
					dir = DIR_NE;
			}
		}
	}
}

void check_borders_collision(HWND hWnd)
{
	RECT temp; //brick/block border

	//ball hitting left border
	if (ball.left <= (border.left + PEN_LEN + BALL_JUMP))
	{
		if (dir == DIR_NW)
			dir = DIR_NE;
		else
			dir = DIR_SE;
	}

	//ball hitting top border
	if (ball.top <= (border.top + PEN_LEN + BALL_JUMP))
	{
		if (dir == DIR_NW)
			dir = DIR_SW;
		else
			dir = DIR_SE;
	}

	//ball hitting right border
	if (ball.right >= (border.right - PEN_LEN - BALL_JUMP))
	{
		if (dir == DIR_NE)
			dir = DIR_NW;
		else
			dir = DIR_SW;
	}

	//ball hitting bottom border...so lost life
	if (ball.bottom >= (border.bottom - PEN_LEN - BALL_JUMP))
	{
		//reset direction to south west when ball starts again
		dir = DIR_SW;
		running = FALSE;
		InvalidateRect(hWnd, &ball, TRUE);
		set_info = TRUE;
		lives--;
		if (lives == 0)
		{
			MessageBox(hWnd, "You lost", "Game Over", MB_OK);
			PostQuitMessage(0);
		}

		//reset ball location
		GetClientRect(hWnd, &temp);
		ball.top = temp.bottom - BALL_DIAMETER - BALL_START_HT;
		ball.bottom = temp.bottom - BALL_START_HT;
		ball.left = (temp.right / 2) - (BALL_DIAMETER / 2);
		ball.right = (temp.right / 2) + (BALL_DIAMETER / 2);
	}
}

//if true, return x, y coordinates within blocks where top left is 0,0
BOOL check_ball_within_block_range(int * x, int * y)
{
	BOOL status = FALSE;
	int range_left = border.left + BLOCK_OFFSET + PEN_LEN;
	int range_right = range_left + (BLOCK_X * BLOCK_WIDTH) + (BLOCK_X * BLOCK_DIV);
	int range_top = border.top + BLOCK_OFFSET;
	int range_bottom = range_top + (BLOCK_Y * BLOCK_HEIGHT) + (BLOCK_Y * BLOCK_DIV);

	//check to see the ball is between the left and right sides and the top and bottom of the block range
	if ((ball.right >= range_left) && (ball.left <= range_right) && (ball.bottom >= range_top) && (ball.top <= range_bottom))
	{
		status = TRUE;
		*x = ball.left - range_left;
		*y = ball.top - range_top;
	}

	return status;
}

//set a temporary RECT in place of current RECT in order to invalidate it, since we didn't store a handle to it
void get_block_rect(RECT * temp, int x, int y)
{
	temp->left = border.left + BLOCK_OFFSET + (BLOCK_WIDTH * x) + (BLOCK_DIV * x);
	temp->right = temp->left + BLOCK_WIDTH;
	temp->top = border.top + BLOCK_OFFSET + (BLOCK_HEIGHT * y) + (BLOCK_DIV * y);
	temp->bottom = temp->top + BLOCK_HEIGHT;
}

//get the corner of the ball in relation to the block to determine which side it makes contact with
void get_distance_to_block(RECT temp, int * x, int * y)
{
	//get distance of the ball to the block in x and y directions
	if (dir == DIR_NE)
	{
		*x = temp.left - ball.right;
		*y = ball.top - temp.bottom;
	}
	else if (dir == DIR_NW)
	{
		*x = ball.left - temp.right;
		*y = ball.top - temp.bottom;
	}
	else if (dir == DIR_SE)
	{
		*x = temp.left - ball.right;
		*y = temp.top - ball.bottom;
	}
	else if (dir == DIR_SW)
	{
		*x = ball.left - temp.right;
		*y = temp.top - ball.bottom;
	}

	//*x = abs(*x);
	//*y = abs(*y);
}

void ball_deflection(RECT temp, int x, int y)
{
	BALL_DIRECTION orig_dir = dir;

	//handle deflection by comparing delta x and y values
	//adjust ball offset to block edge
	if (dir == DIR_NE)
	{
		if (x == y)
		{
			dir = DIR_SW;
			ball.top += BALL_JUMP; ball.bottom += BALL_JUMP;
			ball.left -= BALL_JUMP; ball.right -= BALL_JUMP;
		}
		else if (x > y)
		{
			dir = DIR_NW;
			ball.left -= BALL_JUMP; ball.right -= BALL_JUMP;
		}
		else
		{
			dir = DIR_SE;
			ball.top += BALL_JUMP; ball.bottom += BALL_JUMP;
		}
	}
	else if (dir == DIR_NW)
	{
		if (x == y)
		{
			dir = DIR_SE;
			ball.left += BALL_JUMP; ball.right += BALL_JUMP;
			ball.top += BALL_JUMP; ball.bottom += BALL_JUMP;
		}
		else if (x > y)
		{
			dir = DIR_NE;
			ball.left += BALL_JUMP; ball.right += BALL_JUMP;
		}
		else
		{
			dir = DIR_SW;
			ball.top += BALL_JUMP; ball.bottom += BALL_JUMP;
		}
	}
	else if (dir == DIR_SE)
	{
		if (x == y)
		{
			dir = DIR_NW;
			ball.top -= BALL_JUMP; ball.bottom -= BALL_JUMP;
			ball.left -= BALL_JUMP; ball.right -= BALL_JUMP;
		}
		else if (x > y)
		{
			dir = DIR_SW;
			ball.right -= BALL_JUMP; ball.left -= BALL_JUMP;
		}
		else
		{
			dir = DIR_NE;
			ball.bottom -= BALL_JUMP; ball.top -= BALL_JUMP;
		}
	}
	else if (dir == DIR_SW)
	{
		if (x == y)
		{
			dir = DIR_NE;
			ball.top -= BALL_JUMP; ball.bottom -= BALL_JUMP;
			ball.left += BALL_JUMP; ball.right += BALL_JUMP;
		}
		else if (x > y)
		{
			dir = DIR_SE;
			ball.left += BALL_JUMP; ball.right += BALL_JUMP;
		}
		else
		{
			dir = DIR_NW;
			ball.bottom -= BALL_JUMP; ball.top -= BALL_JUMP;
		}
	}
}

//find which block was hit, set RECT to that block, invalidate the RECT
void handle_collision_with_block(HWND hWnd, int x, int y)
{
	//get block in terms of 2d array x,y
	int x_block = x / (BLOCK_WIDTH + BLOCK_DIV);
	int y_block = y / (BLOCK_HEIGHT + BLOCK_DIV);
	//get x, y in block where ball its
	int x_diff = 0, y_diff = 0;
	RECT temp;

	//if block hasn't already been hit
	if (blocks[x_block][y_block])
	{
		//set flag to false so we know its been hit
		blocks[x_block][y_block] = FALSE;
		//get coordinates of block as RECT
		get_block_rect(&temp, x_block, y_block);
		get_distance_to_block(temp, &x_diff, &y_diff);
		ball_deflection(temp, x_diff, y_diff);
		InvalidateRect(hWnd, &temp, TRUE);
	}
}

void check_bricks_collision(HWND hWnd)
{
	int x = 0, y = 0;
	
	if (check_ball_within_block_range(&x, &y) == TRUE)
	{
		handle_collision_with_block(hWnd, x, y);
	}
}

void move_ball(HWND hWnd)
{
	check_panel_collision();
	check_borders_collision(hWnd);

	switch (dir)
	{
	case DIR_SW:
		ball.left -= BALL_JUMP;
		ball.bottom += BALL_JUMP;
		check_bricks_collision(hWnd);
		InvalidateRect(hWnd, &ball, TRUE);
		ball.top += BALL_JUMP;
		ball.right -= BALL_JUMP;
		break;
	case DIR_NW:
		ball.top -= BALL_JUMP;
		ball.left -= BALL_JUMP;
		check_bricks_collision(hWnd);
		InvalidateRect(hWnd, &ball, TRUE);
		ball.bottom -= BALL_JUMP;
		ball.right -= BALL_JUMP;
		break;
	case DIR_NE:
		ball.top -= BALL_JUMP;
		ball.right += BALL_JUMP;
		check_bricks_collision(hWnd);
		InvalidateRect(hWnd, &ball, TRUE);
		ball.bottom -= BALL_JUMP;
		ball.left += BALL_JUMP;
		break;
	case DIR_SE:
		ball.bottom += BALL_JUMP;
		ball.right += BALL_JUMP;
		check_bricks_collision(hWnd);
		InvalidateRect(hWnd, &ball, TRUE);
		ball.left += BALL_JUMP;
		ball.top += BALL_JUMP;
		break;
	}

	draw_ball = TRUE;
}

void move_panel(HWND hWnd, PANEL_DIRECTION dir)
{
	//in case ball moves at different rate
	int jump = PANEL_JUMP;

	if (dir == SLIDE_LEFT)
	{
		//if panel still has room to move left
		if (panel.left > border.left + PEN_LEN)
		{
			//if there's a gap smaller than standard slide rate
			if ((panel.left - PEN_LEN - border.left) < jump)
				jump = panel.left - PEN_LEN - border.left;

			panel.left -= jump;
			InvalidateRect(hWnd, &panel, TRUE);
			panel.right -= jump;
			draw_panel = TRUE;
		}
	}
	else if (dir == SLIDE_RIGHT)
	{
		//if panel still has room to move right
		if (panel.right < border.right - PEN_LEN)
		{
			//if there's a gap smaller than standard slide rate
			if ((border.right - PEN_LEN - panel.right) < jump)
				jump = border.right - PEN_LEN - panel.right;

			panel.right += jump;
			InvalidateRect(hWnd, &panel, TRUE);
			panel.left += jump;
			draw_panel = TRUE;
		}
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	BALL_DIRECTION prev = dir;
	int i, j;
	HPEN pen;
	TCHAR info[15];
	SIZE info_size;

	switch (message)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		pen = CreatePen(PS_SOLID, PEN_LEN, NULL);
		SelectObject(hdc, pen);

		if (draw_border)
		{
			Rectangle(hdc, border.left, border.top, border.right, border.bottom);
			draw_border = FALSE;
		}
		
		if (draw_panel)
		{
			SelectObject(hdc, GetStockObject(GRAY_BRUSH));
			Rectangle(hdc, panel.left, panel.top, panel.right, panel.bottom);
			draw_panel = FALSE;
		}
		
		if (draw_ball)
		{
			SelectObject(hdc, GetStockObject(BLACK_BRUSH));
			Ellipse(hdc, ball.left, ball.top, ball.right, ball.bottom);
			draw_ball = FALSE;
		}
		
		if (draw_blocks)
		{
			for (i = 0; i < BLOCK_X; i++)
			{
				for (j = 0; j < BLOCK_Y; j++)
				{
					block.left = border.left + BLOCK_OFFSET + (i * BLOCK_WIDTH) + (BLOCK_DIV * i);
					block.top = border.top + BLOCK_OFFSET + (j * BLOCK_HEIGHT) + (BLOCK_DIV * j);
					block.right = block.left + BLOCK_WIDTH;
					block.bottom = block.top + BLOCK_HEIGHT;

					if (blocks[i][j])
						FillRect(hdc, &block, CreateSolidBrush(BRICK_RED));
					else
						ValidateRect(hWnd, &block);
				}
			}
			draw_blocks = FALSE;
		}

		if (set_info)
		{
			wsprintf(info, "Lives: %d", lives);
			GetTextExtentPoint(hdc, info, strlen(info)/sizeof(info[0]), &info_size);
			SetBkMode(hdc, TRANSPARENT);
			SelectObject(hdc, GetStockObject(NULL_BRUSH));
			CreateWindowEx(NULL, "STATIC", info, WS_CHILD | WS_VISIBLE | DFCS_TRANSPARENT, border.left + BLOCK_OFFSET, border.bottom + BLOCK_OFFSET/2, info_size.cx, info_size.cy, hWnd, NULL, NULL, NULL);
			set_info = FALSE;
		}
		
		EndPaint(hWnd, &ps);
		break;
	case WM_KEYDOWN:
		switch(wParam)
		{
		case VK_LEFT:
			move_panel(hWnd, SLIDE_LEFT);
			break;
		case VK_RIGHT:
			move_panel(hWnd, SLIDE_RIGHT);
			break;
		}
		break;
	case WM_CHAR:
		switch(wParam) 
		{
		case _T('A'):
		case _T('a'):
			move_panel(hWnd, SLIDE_LEFT);
			break;
		case _T('D'):
		case _T('d'):
			if (panel.right < border.right)
				move_panel(hWnd, SLIDE_RIGHT);
			break;
		case _T('s'):
		case _T(' '):
			if (running)
				running = FALSE;
			else
				running = TRUE;
			break;
		default:
			break;
		}
		break;
	case WM_TIMER:
		if (running)
			move_ball(hWnd);
		break;
	case WM_SIZE:
		draw_blocks = TRUE;
		draw_border = TRUE;
		draw_panel = TRUE;
		draw_ball = TRUE;
		break;
	case WM_DESTROY:
		KillTimer(hWnd, BALL_FREQ);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

	return (LRESULT)NULL;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wcex;
	RECT windowRect, screenRect;
	LONG diff;
	int i, j;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	if (!RegisterClassEx(&wcex))
	{
		MessageBox(NULL, _T("Call to RegisterClassEx failed!"), _T("Win32 Guided Tour"), NULL);
		return 1;
	}

	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, SCREEN_WIDTH, SCREEN_HEIGHT, NULL, NULL, hInstance, NULL);
	if (!hWnd)
	{
		MessageBox(NULL, _T("Call to CreateWindow failed!"), _T("Win32 Guided Tour"), NULL);
		return 1;
	}

	//user trigger start
	running = FALSE;
	draw_blocks = TRUE;
	draw_border = TRUE;
	draw_panel = TRUE;
	draw_ball = TRUE;
	set_info = TRUE;
	lives = 3;

	GetWindowRect(hWnd, &windowRect);
	GetClientRect(hWnd, &screenRect);
	diff = (windowRect.right - windowRect.left) - screenRect.right;

	//border that panel and ball must stay within
	border.top = BORDER_OFFSET;
	border.bottom = screenRect.bottom - BORDER_OFFSET;
	border.left = screenRect.left + BORDER_OFFSET;
	border.right = screenRect.right - BORDER_OFFSET;

	//panel starting position
	panel.top = screenRect.bottom - BORDER_OFFSET - PANEL_HEIGHT;
	panel.bottom = screenRect.bottom - BORDER_OFFSET - PEN_LEN;
	panel.left = ((screenRect.right / 2) + (diff / 2)) - (PANEL_WIDTH / 2);
	panel.right = ((screenRect.right / 2) + (diff / 2)) + (PANEL_WIDTH / 2);

	//ball starting position
	ball.top = screenRect.bottom - BALL_DIAMETER - BALL_START_HT;
	ball.bottom = screenRect.bottom - BALL_START_HT;
	ball.left = (screenRect.right / 2) - (BALL_DIAMETER / 2);
	ball.right = (screenRect.right / 2) + (BALL_DIAMETER / 2);

	//set all blocks
	for (i = 0; i < BLOCK_X; i++)
		for (j = 0; j < BLOCK_Y; j++)
			blocks[i][j] = TRUE;

	SetTimer(hWnd, 0, BALL_FREQ, NULL);
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}